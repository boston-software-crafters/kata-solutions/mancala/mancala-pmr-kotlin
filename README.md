# Mancala with Kotlin and Gradle

A project that solves the Mancala kata using Kotlin and Gradle with IntelliJ implemented after the May 11th, 20020 Boston Software Crafters meetup event.

# Versions

- Kotlin: 1.3.72
- Gradle: 6.4
- IntelliJ IDEA Community Edition 2020.1
- Kotlin Test with JUnit 5: 1.3.72

# Setup

1. Install [IntelliJ IDEA Communitty Edition](https://www.jetbrains.com/idea/download/#section=mac).
2. Install [Java JDK 8+](https://nodejs.org/en/download/).
3. Clone this repo and `cd` into the created directory
4. Open IntelliJ and import the cloned repo using the top level file: `build.kts.gradle`
5. Start running tests using the file `solution/src/test/kotlin/MainTest.kt` by clicking on the green buttons.

# Mancala

An ancient game of pits and stones.

## How to play the game ([video](https://youtu.be/OX7rj93m6o8))

# The Rules of Mancala
* The Mancala board is made up of two rows of six pockets each (also called “holes,” or, preferably, “pits”)
* Four stones are placed in each of the 12 pockets
* Each player has a “store” (also called a “Mancala”) to their right side of the Mancala board.
* The object of the game is to collect the most pieces by the end of the game.
* The game begins with one player picking up all of the pieces in any one of the pockets on his/her side.
* Moving counter-clockwise, the player deposits one of the stones in each pocket until the stones run out.
* If you run into your own Mancala (store), deposit one piece in it. If you run into your opponent's Mancala, skip it and continue moving to the next pocket.
* If the last piece you drop is in your own Mancala, you take another turn.
* If the last piece you drop is in an empty pocket on your side, you capture that piece and any pieces in the pocket directly opposite
* Always place all captured pieces in your Mancala (store).
* The game ends when all six pockets on one side of the Mancala board are empty.
* The player who still has pieces on his/her side of the board when the game ends captures all of those pieces.
* Count all the pieces in each Mancala. The winner is the player with the most pieces.

### Try it
[play mancala free online](https://arktiv.site/mancala/)
[multiplayer](https://mancala.playdrift.com/)

## The Kata
The goal of the kata is simple: Implement the Rules of Mancala

## My Solution Story

The suggested approach of using a "board" representation of Mancala where the board is modeled as an array of 14 elements did not resonate with me. Rather, I saw the board as a front-end artifact not relevant for a back-end game engine.

What did resonate for me was a notion of having a Game class. A game consists of two Player objects where each player has seven pits. This was my starting point and it has worked well.

First, I set up TDD tests for error conditions and then focused on the rules listed above, attempting to come up with tests for each one.

As always seems to happen, I got carried away writing code and resorted to using my trusted friend, Code Coverage, to flesh out the last few tests until I reached 100% code coverage and a test for every rule.

My initial estimate for the implementation was 8 hours.  The actual time spent was probably closer to 10 hours. An enjoyable 10 hours it was!

Thanks to facilitators Sam Backus and David Sturgis for providing a well run and challenging Mancala Kata experience.

## Resources and References:
* [How to play mancala video](https://youtu.be/OX7rj93m6o8)
* [play mancala free online](https://mancala.playdrift.com/)
* [the wikipedia article](https://en.wikipedia.org/wiki/Mancala)
* [team kotlin project](https://gitlab.com/boston-software-crafters/kata-explorations/mancala/kotlin/mancala-team-kotlin.git)

![Mancala board](https://upload.wikimedia.org/wikipedia/commons/4/42/Thai-mancala-board-possibly-main-chakot-or-mak-khom.jpg)
